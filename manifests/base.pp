class base {
	exec { "apt-update":
		command => "apt-get -y update",
		path => "/usr/bin"
	}

	package {
		["mysql-client", "mysql-server", "libmysqlclient-dev"]:
		ensure => installed, require => Exec['apt-update']
	}

	package { "php5":
		ensure => present
	}

	package { "php5-mysql":
		ensure => present
	}

	package { "libapache2-mod-php5":
		ensure => present,
	}

	package { "apache2":
		ensure => present,
	}

	service { "apache2":
		ensure => running,
		require => Package["apache2"]
	}

	group { "puppet":
	    ensure => "present",
	}

	exec { "restart-apache":
		command => "apache2 restart",
		path => "/etc/init.d"
	}
}

include base
